package com.alexandre.estoque.controller;

import com.alexandre.estoque.domain.TipoProdutoEnum;
import com.alexandre.estoque.domain.VConsultaProduto;
import com.alexandre.estoque.dto.ConsultaProdutoLucroDTO;
import com.alexandre.estoque.dto.ConsultaProdutoSaidaDTO;
import com.alexandre.estoque.service.ConsultaProdutoService;
import com.alexandre.estoque.util.ModelMapperUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/consulta")
public class ConsultaProdutoController {

    @Autowired
    private ConsultaProdutoService service;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/saida/{tipo}")
    public List<ConsultaProdutoSaidaDTO> findSaidaByTipo(@PathVariable TipoProdutoEnum tipo) {
        log.info("Executando findSaidaByTipo");
        List<VConsultaProduto> listConsultaProduto = this.service.findByTipo(tipo);
        return this.modelMapperUtil.mapList(listConsultaProduto, ConsultaProdutoSaidaDTO.class);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/saida")
    public List<ConsultaProdutoSaidaDTO> findAllSaida() {
        log.info("Executando findAllSaida");
        List<VConsultaProduto> listConsultaProduto = this.service.findAll();
        return this.modelMapperUtil.mapList(listConsultaProduto, ConsultaProdutoSaidaDTO.class);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/lucro/{tipo}")
    public List<ConsultaProdutoLucroDTO> findLucroByTipo(@PathVariable TipoProdutoEnum tipo) {
        log.info("Executando findLucroByTipo");
        List<VConsultaProduto> listConsultaProduto = this.service.findByTipo(tipo);
        return this.modelMapperUtil.mapList(listConsultaProduto, ConsultaProdutoLucroDTO.class);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/lucro")
    public List<ConsultaProdutoLucroDTO> findAllLucro() {
        log.info("Executando findAllLucro");
        List<VConsultaProduto> listConsultaProduto = this.service.findAll();
        return this.modelMapperUtil.mapList(listConsultaProduto, ConsultaProdutoLucroDTO.class);
    }
}
