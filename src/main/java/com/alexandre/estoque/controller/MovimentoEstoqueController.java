package com.alexandre.estoque.controller;

import com.alexandre.estoque.domain.MovimentoEstoque;
import com.alexandre.estoque.service.MovimentoEstoqueService;
import com.alexandre.estoque.dto.IdDTO;
import com.alexandre.estoque.dto.MovimentoEstoqueDTO;
import com.alexandre.estoque.util.ModelMapperUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/movimentoestoque")
public class MovimentoEstoqueController {

    @Autowired
    private MovimentoEstoqueService service;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public IdDTO create(@RequestBody @Valid MovimentoEstoqueDTO dto) {
        log.info("Executando create");
        MovimentoEstoque movimentoEstoque = this.modelMapperUtil.map(dto, MovimentoEstoque.class);
        movimentoEstoque = this.service.create(movimentoEstoque);
        return this.modelMapperUtil.map(movimentoEstoque, IdDTO.class);
    }
}
