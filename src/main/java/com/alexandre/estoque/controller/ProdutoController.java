package com.alexandre.estoque.controller;

import com.alexandre.estoque.dto.PageDTO;
import com.alexandre.estoque.dto.PageRequestDTO;
import com.alexandre.estoque.dto.ProdutoDTO;
import com.alexandre.estoque.exception.NotFoundException;
import com.alexandre.estoque.service.ProdutoService;
import com.alexandre.estoque.domain.Produto;
import com.alexandre.estoque.dto.IdDTO;
import com.alexandre.estoque.util.ModelMapperUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService service;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public PageDTO<ProdutoDTO> findAll(PageRequestDTO pageRequestDTO) {
        log.info("Executando findAll");
        PageRequest pageReq = pageRequestDTO.getPageRequest(Sort.Direction.ASC, "codigo");
        Page<Produto> page = this.service.findAll(pageReq);
        return this.modelMapperUtil.mapPage(page, ProdutoDTO.class);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{id}")
    public ProdutoDTO findById(@PathVariable Long id) {
        log.info("Executando findById");
        Produto produto = this.service.findById(id);
        return this.modelMapperUtil.map(produto, ProdutoDTO.class);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/codigo/{codigo}")
    public ProdutoDTO findByCodigo(@PathVariable String codigo) {
        log.info("Executando findByCodigo");
        Produto produto = this.service.findByCodigo(codigo);
        return this.modelMapperUtil.map(produto, ProdutoDTO.class);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public IdDTO create(@RequestBody @Valid ProdutoDTO dto) {
        log.info("Executando create");
        Produto produto = this.modelMapperUtil.map(dto, Produto.class);
        produto = this.service.create(produto);
        return this.modelMapperUtil.map(produto, IdDTO.class);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping
    public IdDTO update(@RequestBody @Valid ProdutoDTO dto) {
        log.info("Executando update");
        boolean found = this.service.existsById(dto.getId());
        if (found) {
            Produto produto = this.modelMapperUtil.map(dto, Produto.class);
            produto = this.service.save(produto);
            return this.modelMapperUtil.map(produto, IdDTO.class);
        } else {
            throw new NotFoundException("Produto");
        }
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Long id) {
        log.info("Executando delete");
        this.service.inativarById(id);
    }
}
