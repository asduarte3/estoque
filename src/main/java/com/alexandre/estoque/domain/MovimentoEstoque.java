package com.alexandre.estoque.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id"})
@Entity
@Table(name = "mov_estoque")
public class MovimentoEstoque {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_produto")
    private Produto produto;

    @Column(name = "tipo")
    @Enumerated(EnumType.STRING)
    @NotNull
    private TipoMovimentoEnum tipo;

    @Column(name = "valor")
    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer = 10, fraction = 2)
    private Double valor;

    @Column(name = "data")
    @NotNull
    private LocalDateTime data;

    @Column(name = "quantidade")
    @Min(value = 1)
    @Max(value = 9999)
    private Integer quantidade;
}
