package com.alexandre.estoque.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "codigo", "descricao"})
@Entity
@Table(name = "produto")
public class Produto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "codigo")
    @NotBlank
    @Size(max = 100)
    private String codigo;

    @Column(name = "descricao")
    @NotBlank
    @Size(max = 250)
    private String descricao;

    @Column(name = "tipo")
    @Enumerated(EnumType.STRING)
    @NotNull
    private TipoProdutoEnum tipo;

    @Column(name = "valor")
    @DecimalMin(value = "0.0", inclusive = false)
    @Digits(integer = 10, fraction = 2)
    private Double valor;

    @Column(name = "quantidade")
    @Min(value = 0)
    @Max(value = 9999)
    private Integer quantidade;

    @Column(name = "ativo")
    private Boolean ativo = true;
}
