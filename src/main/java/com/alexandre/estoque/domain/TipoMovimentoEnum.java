package com.alexandre.estoque.domain;

public enum TipoMovimentoEnum {
    ENTRADA,
    SAIDA
}
