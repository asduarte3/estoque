package com.alexandre.estoque.domain;

public enum TipoProdutoEnum {
    ELETRONICO,
    ELETRODOMESTICO,
    MOVEL
}
