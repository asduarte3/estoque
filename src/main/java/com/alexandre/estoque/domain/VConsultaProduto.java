package com.alexandre.estoque.domain;

import lombok.*;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id"})
@Entity
@Immutable
@Table(name = "v_consulta_produto")
public class VConsultaProduto {
    @Id
    @Column(name = "id", insertable = false, updatable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_produto", insertable = false, updatable = false)
    private Produto produto;

    @Column(name = "tipo", insertable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private TipoProdutoEnum tipo;

    @Column(name = "quantidade_disponivel", insertable = false, updatable = false)
    private Integer quantidadeDisponivel;

    @Column(name = "quantidade_saida", insertable = false, updatable = false)
    private Integer quantidadeSaida;

    @Column(name = "valor_lucro", insertable = false, updatable = false)
    private Double valorLuro;
}
