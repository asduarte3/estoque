package com.alexandre.estoque.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsultaProdutoSaidaDTO {
    private Long id;

    private ProdutoDTO produto;

    private Integer quantidadeDisponivel;

    private Integer quantidadeSaida;
}
