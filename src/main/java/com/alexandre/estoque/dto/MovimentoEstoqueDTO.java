package com.alexandre.estoque.dto;

import com.alexandre.estoque.domain.TipoMovimentoEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MovimentoEstoqueDTO {
    @NotNull
    private IdDTO produto;

    @NotNull
    private TipoMovimentoEnum tipo;

    private Double valor;

    private Integer quantidade;
}
