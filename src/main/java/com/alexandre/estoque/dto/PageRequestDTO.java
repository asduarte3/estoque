package com.alexandre.estoque.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PageRequestDTO {
    public static final int DEFAULT_SIZE = 10;

    @Builder.Default
    private int page = 0;

    @Builder.Default
    private int size = DEFAULT_SIZE;

    public PageRequest getPageRequest(Sort.Direction direction, String property) {
        return PageRequest.of(
                this.getPage(),
                this.getSize(),
                direction, property);
    }
}
