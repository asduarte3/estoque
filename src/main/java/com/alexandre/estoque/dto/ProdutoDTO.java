package com.alexandre.estoque.dto;

import com.alexandre.estoque.domain.TipoProdutoEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProdutoDTO {
    private Long id;

    private String codigo;

    private String descricao;

    private TipoProdutoEnum tipo;

    private Double valor;

    private Integer quantidade;

    private Boolean ativo;
}
