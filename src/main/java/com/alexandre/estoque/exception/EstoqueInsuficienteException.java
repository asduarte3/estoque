package com.alexandre.estoque.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class EstoqueInsuficienteException extends ResponseStatusException {
    public EstoqueInsuficienteException() {
        super(HttpStatus.BAD_REQUEST, "Estoque insuficiente para a movimentação");
    }
}
