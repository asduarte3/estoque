package com.alexandre.estoque.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NotFoundException extends ResponseStatusException {
    public NotFoundException(String entity) {
        super(HttpStatus.NOT_FOUND, "Registro não encontrado: " + entity);
    }
}
