package com.alexandre.estoque.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ExceptionResponse> errorHandling(Throwable t) {
        if (t instanceof ResponseStatusException) {
            ResponseStatusException exception = (ResponseStatusException) t;
            return new ResponseEntity<>(new ExceptionResponse(exception.getReason()), exception.getStatus());
        } else {
            log.error("", t);
            return new ResponseEntity<>(new ExceptionResponse(t.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}
