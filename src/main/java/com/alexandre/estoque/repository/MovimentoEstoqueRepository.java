package com.alexandre.estoque.repository;

import com.alexandre.estoque.domain.MovimentoEstoque;
import org.springframework.data.repository.CrudRepository;

public interface MovimentoEstoqueRepository extends CrudRepository<MovimentoEstoque, Long> {
}
