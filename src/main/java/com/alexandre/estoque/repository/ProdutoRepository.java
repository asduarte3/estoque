package com.alexandre.estoque.repository;

import com.alexandre.estoque.domain.Produto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface ProdutoRepository extends PagingAndSortingRepository<Produto, Long> {
    Page<Produto> findByAtivoTrue(Pageable pageable);

    Optional<Produto> findByCodigoAndAtivoTrue(String codigo);
}
