package com.alexandre.estoque.repository;

import com.alexandre.estoque.domain.TipoProdutoEnum;
import com.alexandre.estoque.domain.VConsultaProduto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VConsultaProdutoRepository extends CrudRepository<VConsultaProduto, Long> {
    @Query("SELECT v FROM VConsultaProduto v join fetch v.produto p WHERE v.tipo = :tipo order by p.codigo")
    List<VConsultaProduto> findByTipo(TipoProdutoEnum tipo);

    @Query("SELECT v FROM VConsultaProduto v join fetch v.produto p order by p.codigo")
    List<VConsultaProduto> findAll();
}
