package com.alexandre.estoque.service;

import com.alexandre.estoque.domain.TipoProdutoEnum;
import com.alexandre.estoque.domain.VConsultaProduto;
import com.alexandre.estoque.repository.VConsultaProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ConsultaProdutoService {

    @Autowired
    private VConsultaProdutoRepository vProdutoLucrorepository;

    public List<VConsultaProduto> findByTipo(TipoProdutoEnum tipo) {
        return this.vProdutoLucrorepository.findByTipo(tipo);
    }

    public List<VConsultaProduto> findAll() {
        return this.vProdutoLucrorepository.findAll();
    }

}
