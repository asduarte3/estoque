package com.alexandre.estoque.service;

import com.alexandre.estoque.domain.MovimentoEstoque;
import com.alexandre.estoque.domain.Produto;
import com.alexandre.estoque.domain.TipoMovimentoEnum;
import com.alexandre.estoque.exception.EstoqueInsuficienteException;
import com.alexandre.estoque.repository.MovimentoEstoqueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class MovimentoEstoqueService {

    @Autowired
    private MovimentoEstoqueRepository repository;

    @Autowired
    private ProdutoService produtoService;

    public MovimentoEstoque create(MovimentoEstoque movimentoEstoque) {
        Produto produto = this.movimentarEstoqueProduto(movimentoEstoque);
        movimentoEstoque.setId(null);
        movimentoEstoque.setProduto(produto);
        movimentoEstoque.setData(LocalDateTime.now());
        return this.repository.save(movimentoEstoque);
    }

    private Produto movimentarEstoqueProduto(MovimentoEstoque movimentoEstoque) {
        Produto produto = this.produtoService.findById(movimentoEstoque.getProduto().getId());
        int quantidade = TipoMovimentoEnum.SAIDA.equals(movimentoEstoque.getTipo()) ? -(movimentoEstoque.getQuantidade()) : movimentoEstoque.getQuantidade();
        produto.setQuantidade(produto.getQuantidade() + quantidade);
        if (produto.getQuantidade() >= 0) {
            return this.produtoService.save(produto);
        } else {
            throw new EstoqueInsuficienteException();
        }
    }
}
