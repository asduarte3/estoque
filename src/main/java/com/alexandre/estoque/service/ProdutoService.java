package com.alexandre.estoque.service;

import com.alexandre.estoque.domain.Produto;
import com.alexandre.estoque.exception.NotFoundException;
import com.alexandre.estoque.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProdutoService {

    @Autowired
    private ProdutoRepository repository;

    public Produto create(Produto produto) {
        produto.setId(null);
        produto.setAtivo(true);
        return this.save(produto);
    }

    public Produto save(Produto produto) {
        return this.repository.save(produto);
    }

    public Page<Produto> findAll(Pageable pageable) {
        return this.repository.findByAtivoTrue(pageable);
    }

    public Produto findById(Long id) {
        return this.repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Produto"));
    }

    public Produto findByCodigo(String codigo) {
        return this.repository.findByCodigoAndAtivoTrue(codigo)
                .orElseThrow(() -> new NotFoundException("Produto"));
    }

    public boolean existsById(Long id) {
        return this.repository.existsById(id);
    }

    public void inativarById(Long id) {
        Produto produto = this.findById(id);
        produto.setAtivo(false);
        this.save(produto);
    }
}
