package com.alexandre.estoque.util;

import com.alexandre.estoque.dto.PageDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ModelMapperUtil {
    @Autowired
    private ModelMapper modelMapper;

    public <T> T map(Object source, Class<T> destinationType) {
        return this.modelMapper.map(source, destinationType);
    }

    public <S, T> PageDTO<T> mapPage(Page<S> page, Class<T> contentClass) {
        PageDTO<T> pageDTO = this.modelMapper.map(page, PageDTO.class);
        if (!CollectionUtils.isEmpty(page.getContent())) {
            pageDTO.setDataList(this.mapList(page.getContent(), contentClass));
        }
        return pageDTO;
    }

    public <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        return source
                .stream()
                .map(element -> this.modelMapper.map(element, targetClass))
                .collect(Collectors.toList());
    }
}
