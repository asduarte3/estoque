DROP VIEW IF EXISTS v_consulta_produto;
DROP TABLE IF EXISTS mov_estoque;
DROP TABLE IF EXISTS produto;

CREATE TABLE produto (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  codigo VARCHAR(100) NOT NULL UNIQUE,
  descricao VARCHAR(250) NOT NULL,
  tipo VARCHAR(100) NOT NULL,
  valor DECIMAL(10,2) NOT NULL,
  quantidade INT NOT NULL,
  ativo INT NOT NULL
);

CREATE TABLE mov_estoque (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  id_produto BIGINT NOT NULL,
  tipo VARCHAR(100) NOT NULL,
  valor DECIMAL(20,2) NOT NULL,
  data TIMESTAMP NOT NULL,
  quantidade INT NOT NULL,
  foreign key (id_produto) references produto(id)
);

CREATE VIEW v_consulta_produto AS
select
p.id,
p.id as id_produto,
p.tipo,
p.quantidade as quantidade_disponivel,
sum(coalesce(m.quantidade,0)) as quantidade_saida,
sum((coalesce(m.valor,0) * nvl2(m.id, m.quantidade, 0)) - (nvl2(m.id, p.valor, 0) * nvl2(m.id, m.quantidade, 0))) as valor_lucro
from produto p
left join mov_estoque m on (m.id_produto = p.id and m.tipo = 'SAIDA')
where p.ativo = 1
group by
p.id, p.tipo;