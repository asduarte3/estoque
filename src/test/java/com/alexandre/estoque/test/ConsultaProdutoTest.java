package com.alexandre.estoque.test;

import com.alexandre.estoque.domain.TipoProdutoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ConsultaProdutoTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void testFindAllSaida() throws Exception {
        this.mockMvc.perform(get("/consulta/saida")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void testFindSaidaByTipo() throws Exception {
        this.mockMvc.perform(get("/consulta/saida/" + TipoProdutoEnum.MOVEL.name())).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void testFindAllLucro() throws Exception {
        this.mockMvc.perform(get("/consulta/lucro")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void testFindLucroByTipo() throws Exception {
        this.mockMvc.perform(get("/consulta/lucro/" + TipoProdutoEnum.MOVEL.name())).andDo(print()).andExpect(status().isOk());
    }
}