package com.alexandre.estoque.test;

import com.alexandre.estoque.domain.Produto;
import com.alexandre.estoque.domain.TipoMovimentoEnum;
import com.alexandre.estoque.dto.IdDTO;
import com.alexandre.estoque.dto.MovimentoEstoqueDTO;
import com.alexandre.estoque.util.ModelMapperUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class MovimentoEstoqueTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProdutoHelper helper;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Test
    void testCreate() throws Exception {
        Produto produto = this.helper.create();
        MovimentoEstoqueDTO dto = MovimentoEstoqueDTO.builder()
                .produto(new IdDTO(produto.getId()))
                .tipo(TipoMovimentoEnum.ENTRADA)
                .quantidade(5)
                .valor(100d)
                .build();

        this.mockMvc.perform(
                post("/movimentoestoque")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(dto))
        )
                .andDo(print()).andExpect(status().isCreated());
    }
}