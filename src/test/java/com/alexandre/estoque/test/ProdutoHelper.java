package com.alexandre.estoque.test;

import com.alexandre.estoque.domain.Produto;
import com.alexandre.estoque.domain.TipoProdutoEnum;
import com.alexandre.estoque.dto.ProdutoDTO;
import com.alexandre.estoque.service.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class ProdutoHelper {
    @Autowired
    private ProdutoService service;

    public Produto create() {
        String codigo = String.valueOf(System.currentTimeMillis());
        Produto produto = Produto.builder()
                .codigo(codigo)
                .descricao("produto " + codigo)
                .tipo(TipoProdutoEnum.ELETRONICO)
                .quantidade(10)
                .valor(100d)
                .build();
        produto = this.service.create(produto);
        return produto;
    }

    public ProdutoDTO createDTO() {
        String codigo = String.valueOf(System.currentTimeMillis());
        ProdutoDTO dto = ProdutoDTO.builder()
                .codigo(codigo)
                .descricao("produto " + codigo)
                .tipo(TipoProdutoEnum.ELETRONICO)
                .quantidade(10)
                .valor(100d)
                .build();
        return dto;
    }
}