package com.alexandre.estoque.test;

import com.alexandre.estoque.domain.Produto;
import com.alexandre.estoque.dto.ProdutoDTO;
import com.alexandre.estoque.util.ModelMapperUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ProdutoTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProdutoHelper helper;

    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Test
    void testFindAll() throws Exception {
        this.mockMvc.perform(get("/produtos")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void testFindById() throws Exception {
        Produto produto = this.helper.create();
        this.mockMvc.perform(get("/produtos/" + produto.getId())).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void testFindByCodigo() throws Exception {
        Produto produto = this.helper.create();
        this.mockMvc.perform(get("/produtos/codigo/" + produto.getCodigo())).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void testCreate() throws Exception {
        ProdutoDTO dto = this.helper.createDTO();
        this.mockMvc.perform(
                post("/produtos")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(dto))
        )
                .andDo(print()).andExpect(status().isCreated());
    }

    @Test
    void testUpdate() throws Exception {
        Produto produto = this.helper.create();
        ProdutoDTO dto = this.modelMapperUtil.map(produto, ProdutoDTO.class);
        dto.setQuantidade(dto.getQuantidade() + 1);

        this.mockMvc.perform(
                put("/produtos")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(dto))
        )
                .andDo(print()).andExpect(status().isOk());
    }

    @Test
    void testDelete() throws Exception {
        Produto produto = this.helper.create();
        this.mockMvc.perform(delete("/produtos/" + produto.getId())).andDo(print()).andExpect(status().isNoContent());
    }
}